package nz.govt.health.hsaap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import nz.govt.health.hsaap.kafka.producer.ClaimRequestMessageProducer;
import nz.govt.health.hsaap.model.ClaimRequestMessage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.UUID;

@SpringBootApplication
@ComponentScan(basePackages = "nz.govt.health.hsaap")
public class CamundaApplication {

    public static void main(String... args) throws JsonProcessingException, InterruptedException {
        ConfigurableApplicationContext context = SpringApplication.run(CamundaApplication.class, args);

        ClaimRequestMessageProducer producer = context.getBean(ClaimRequestMessageProducer.class);
        ObjectMapper mapper = context.getBean(ObjectMapper.class);


        while (true) {
            ClaimRequestMessage claimRequest = new ClaimRequestMessage(UUID.randomUUID().toString(), UUID.randomUUID().toString());

            String valueAsJson = mapper.writeValueAsString(claimRequest);
            producer.sendMessage(valueAsJson);
            Thread.sleep(10000);
        }

    }

    @Bean
    public ObjectMapper mapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        return mapper;
    }

//    @Bean
//    public ClaimRequestMessageProducer producer() {
//        return new ClaimRequestMessageProducer();
//    }


}