package nz.govt.health.hsaap.model;

import java.time.LocalDateTime;

public class ClaimResponseMessage {

    private String id;
    private String provider;
    private LocalDateTime entered;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public LocalDateTime getEntered() {
        return entered;
    }

    public void setEntered(LocalDateTime entered) {
        this.entered = entered;
    }

}
