package nz.govt.health.hsaap.model;

public class ClaimRequestMessage {

    public ClaimRequestMessage(String id, String provider) {
        this.id = id;
        this.provider = provider;
    }

    private String id;
    private String provider;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

}
