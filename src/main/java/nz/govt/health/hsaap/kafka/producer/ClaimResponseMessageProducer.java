package nz.govt.health.hsaap.kafka.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Configuration("claimResponseMessageProducer")
public class ClaimResponseMessageProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClaimResponseMessageProducer.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Value(value = "${claim.response.topic.name}")
    private String topicName;

    public void sendMessage(String message) {

        ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(topicName, message);

        future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {

            @Override
            public void onSuccess(SendResult<String, String> result) {
                LOGGER.info("Sent claim request message {} with offset {}", message, result.getRecordMetadata().offset());

            }

            @Override
            public void onFailure(Throwable ex) {
                LOGGER.info("Unable to send message {} due to : {}", message, ex.getMessage());
            }
        });
    }

}
