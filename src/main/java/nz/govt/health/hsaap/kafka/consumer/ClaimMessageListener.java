package nz.govt.health.hsaap.kafka.consumer;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class ClaimMessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClaimMessageListener.class);

    @KafkaListener(topics = "${claim.request.topic.name}", containerFactory = "claimConsumerFactory")
    public void claimRequestListener(String request) {
        Map<String, Object> claimRequest = new HashMap<>();
        claimRequest.put("claimRequestPayload", request);
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RuntimeService runtimeService = processEngine.getRuntimeService();
        ProcessInstance instance = runtimeService.startProcessInstanceByKey("claimCreation", claimRequest);
        LOGGER.info("Claim Process Started with process definitation id: {} ", instance.getProcessDefinitionId());
         LOGGER.info("TEST TEST: {} ", instance.getProcessDefinitionId());
    }

}
