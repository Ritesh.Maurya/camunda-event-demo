package nz.govt.health.hsaap.delegates;

import com.fasterxml.jackson.databind.ObjectMapper;
import nz.govt.health.hsaap.entities.Claim;
import nz.govt.health.hsaap.kafka.producer.ClaimResponseMessageProducer;
import nz.govt.health.hsaap.model.ClaimResponseMessage;
import nz.govt.health.hsaap.repository.ClaimRepository;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ClaimRequestProcessingDelegate implements JavaDelegate {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClaimRequestProcessingDelegate.class);

    @Autowired
    private ClaimRepository claimRepository;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    @Qualifier("claimResponseMessageProducer")
    private ClaimResponseMessageProducer claimResponseMessageProducer;


    @Override
    public void execute(DelegateExecution execution) throws Exception {
        LOGGER.info("ClaimRequestProcessingDelegate DELEGATE CALLED");
        boolean claimValid = (boolean) execution.getVariable("claimValid");
        String validClaimRequest = (String) execution.getVariable("validClaimRequest");
        LOGGER.info("value of claimValid in ClaimRequestProcessingDelegate {}", claimValid);
        LOGGER.info("value of validClaimRequest in ClaimRequestProcessingDelegate {}", validClaimRequest);
        if (claimValid && validClaimRequest != null) {
            Claim claim = mapper.readValue(validClaimRequest, Claim.class);
            claim.setDateTime(LocalDateTime.now());
            claimRepository.save(claim);
            LOGGER.info("Claim saved in Postgres database");
            ClaimResponseMessage claimResponseMessage = mapper.convertValue(claim, ClaimResponseMessage.class);
            LOGGER.info("Sending claim created event to kafka");
            claimResponseMessageProducer.sendMessage(mapper.writeValueAsString(claimResponseMessage));
        }
    }
}
