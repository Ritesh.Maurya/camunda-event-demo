package nz.govt.health.hsaap.delegates;

import com.fasterxml.jackson.databind.ObjectMapper;
import nz.govt.health.hsaap.model.ClaimRequest;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ClaimRequestValidationDelegate implements JavaDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClaimRequestValidationDelegate.class);


    @Autowired
    private ObjectMapper mapper;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        LOGGER.info("ClaimRequestValidationDelegate DELEGATE CALLED");
        Map<String, Object> claimCreation = execution.getVariables();
        String claimRequestPayload = (String) claimCreation.get("claimRequestPayload");
        try {
            ClaimRequest claimRequest = mapper.readValue(claimRequestPayload, ClaimRequest.class);
            if (claimRequest != null) {
                execution.setVariable("claimValid", true);
                execution.setVariable("validClaimRequest", mapper.writeValueAsString(claimRequest));
                LOGGER.info("Claim request valid {}", claimRequest);
            }
        } catch (Exception e) {
            LOGGER.info("Claim request invalid {}", claimCreation);
            execution.setVariable("claimValid", false);

        }

    }
}
