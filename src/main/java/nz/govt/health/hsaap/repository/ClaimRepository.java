package nz.govt.health.hsaap.repository;

import nz.govt.health.hsaap.entities.Claim;
import org.springframework.data.repository.CrudRepository;

public interface ClaimRepository extends CrudRepository<Claim, Long> {
}
